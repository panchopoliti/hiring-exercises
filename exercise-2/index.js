/* eslint-disable no-console */
const utilsFunctions = require('./utils-functions.js');
const customerFunctions = require('./customer-functions.js');

const {
  getDataFromJSONFile,
  writeInJSONFile,
} = utilsFunctions;

const {
  getCustomersByCountry,
  getCountryCode,
  createStripeCustomers,
  getInfoFromEachStripeCustomer,
} = customerFunctions;

/*
MAIN EXPLANATION

I splitted my code in as many pure functions as possible to make it clear at a glance what it is doing.
The main approach also was to write simple and reusable functions, even though in this project were only used once.

I decided to use the airbnb LINT to ensure the usage of best practices and easier code reading.
*/

const handler = async (country) => {
  try {
    let finalCustomers = [];

    /* add code below this line */

    getCustomersByCountry(country)
      .then((customersByCountry) => getDataFromJSONFile('./countries-ISO3166.json')
        .then((countriesCode) => getCountryCode(countriesCode, country))
        .then((countryCode) => createStripeCustomers(customersByCountry, countryCode))
        .then((stripeCustomers) => {
          finalCustomers = getInfoFromEachStripeCustomer(
            stripeCustomers,
            ['id: customerId', 'email', 'metadata.country_code: country'],
          );
          writeInJSONFile('final-customers.json', finalCustomers, 'Final Customers written with success');
          console.log(finalCustomers);
        }))
      .catch((err) => console.log(err));

    // filter the customers by country
    // transform customers to save into Stripe
    // for each customer create a Stripe customer
    // push into finalCustomers the stripe customers with email, country and id as properties.
    // write finalCustomers array into final-customers.json using fs
    /*
      finalCustomers array should look like:
      finalCustomers = [{
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        },
        {
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        }
      }]
    */

    /* add code above this line */
  } catch (e) {
    throw e;
  }
};

handler('Spain');
