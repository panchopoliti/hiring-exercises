const Stripe = require('stripe');
const utilsFunctions = require('./utils-functions.js');

// eslint-disable-next-line max-len
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR';
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

/*
In most of the functions that are receiving objects or arrays as params,
there was no need to clone them, because the methods used where directly creating new instances.
*/

const {
  getDataFromJSONFile,
  getDeepValue,
  pushToArrayIfNotIncluded,
} = utilsFunctions;

const getCustomersByCountry = (country) => getDataFromJSONFile('./customers.json')
  .then((customers) => customers.filter((customer) => customer.country === country))
  .catch((err) => console.log(err));

const getCountryCode = (codes, country) => {
  const countryCodesArr = Object.keys(codes);

  return countryCodesArr.find((code) => codes[code] === country);
};

const getCustomerFullName = (customer) => `${customer.first_name} ${customer.last_name}`;

const postStripeCustomer = async (customerObj) => {
  try {
    const customer = await stripe.customers.create(
      {
        description: 'Create new Customer',
        email: customerObj.email,
        name: customerObj.full_name,
        metadata: {
          country_code: customerObj.country_code,
        },
      },
    );

    return customer;
  } catch (e) {
    throw e;
  }
};

const createStripeCustomers = async (customersByCountry, countryCode) => {
  try {
    const stripeCustomers = [];

    customersByCountry.forEach((customer) => {
      const customerName = getCustomerFullName(customer);

      /*
       I create a new object with the full customer data,
      to let the possibility to add more informoation in later Stripe customers if needed.
      */
      const createCustomerObj = {
        ...customer,
        full_name: customerName,
        country_code: countryCode,
      };
      const stripeCustomer = postStripeCustomer(createCustomerObj);
      stripeCustomers.push(stripeCustomer);
    });

    return await Promise.all(stripeCustomers);
  } catch (e) {
    throw e;
  }
};

const getKeyForCustomerObject = (path) => {
  const paths = path.split('.');
  const newKey = paths[paths.length - 1].split(':');

  return newKey[newKey.length - 1].trim();
};

const logUnavailablePaths = (unavailablePaths) => {
  unavailablePaths.forEach((path) => {
    console.log(`The path obj.${path} is not available in the Stripe Customer information`);
  });
};

/*
  The main approach of this function, was to make it easy and reusable.
  It can be flexible to receive any object path in a form of an Array,
  and including the return keyName for the new object.

  At the same time, I wanted to make it easy to debug, logging probable bad usages.
*/

/*
 Usage and documented with JSDoc syntax

 /**
 * Creates new Array of Objects with selected filtered information
 * @param  {Array.<Object>} stripeCustomers The base Objects from where we should take the information
 * @param  {Array.<String>} pathsArr The strings that describe the path to access the required values.
 *  Use "." to access the object and ":" after the path to change the new key name
 * @return {Array.<Object>} with the filtered customer information
 * @example
 *  //Returns [
    {
      customerId: 'cus_IEojXRYhtK6jy4',
      email: 'eavonb@delicious.com',
      country: 'ES'
    },
    [...]
  ]
 * getInfoFromEachStripeCustomer(stripeCustomers, ['id: customerId', 'email', 'metadata.country_code: country']);
 *
 */

const getInfoFromEachStripeCustomer = (stripeCustomers, pathsArr) => {
  let unavailablePaths = [];

  const filteredStripeCustomers = stripeCustomers.map((customer) => {
    const customerInfo = {};

    pathsArr.forEach((path) => {
      const valueFromPath = getDeepValue(customer, path);

      if (!valueFromPath) {
        unavailablePaths = pushToArrayIfNotIncluded(unavailablePaths, path);
        return;
      }

      const key = getKeyForCustomerObject(path);
      customerInfo[key] = valueFromPath;
    });

    return customerInfo;
  });

  if (unavailablePaths.length) logUnavailablePaths(unavailablePaths);

  return filteredStripeCustomers;
};

module.exports = {
  getCustomersByCountry,
  getCountryCode,
  createStripeCustomers,
  getInfoFromEachStripeCustomer,
};
