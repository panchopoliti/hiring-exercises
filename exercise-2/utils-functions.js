const fs = require('fs');
const fsPromises = require('fs').promises;

const getDataFromJSONFile = async (file) => {
  try {
    const data = await fsPromises.readFile(file, 'utf-8');
    const parsedData = JSON.parse(data);

    return parsedData;
  } catch (e) {
    throw e;
  }
};

const getDeepValue = (obj, path) => {
  const paths = path.split('.');
  const removeNewKeyName = paths[paths.length - 1].split(':')[0];
  paths[paths.length - 1] = removeNewKeyName;

  return paths.reduce((acum, elem) => acum[elem], obj);
};

const pushToArrayIfNotIncluded = (arr, elem) => {
  const cloneArr = arr.slice();
  const isElemInArray = cloneArr.includes(elem);

  if (!isElemInArray) {
    cloneArr.push(elem);
  }

  return cloneArr;
};

const handleWriteFileResponse = (err, message) => {
  if (err) console.log(err);

  return console.log(message);
};

const writeInJSONFile = (file, data, successMessage) => {
  const jsonData = JSON.stringify(data);

  fs.writeFile(file, jsonData, 'utf-8', (err) => handleWriteFileResponse(err, successMessage));
};

module.exports = {
  getDataFromJSONFile,
  writeInJSONFile,
  getDeepValue,
  pushToArrayIfNotIncluded,
};

// getDataFromJSONFile written without async/await syntax and without fsPromises
/*
const getDataFromJSONFile = (file) => {
  return new Promise((resolve, reject) => {

    fs.readFile(file, 'utf-8', (err, data) => {
      if (err) reject(err);
      const parsedData = JSON.parse(data);

      resolve(parsedData);
    });
  });
}
*/
