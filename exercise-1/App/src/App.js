import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import { UserProfile, UserPosts } from './components/User';

const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache(),
});

function App() {
  return (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
      <ApolloProvider client={client}>
        <UserProfile />
        <UserPosts />
      </ApolloProvider>
    </div>
  );
}

export default App;
