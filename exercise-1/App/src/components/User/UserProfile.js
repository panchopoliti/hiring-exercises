/* eslint-disable react/prop-types */

import React from 'react';
import { gql } from '@apollo/client';
import { ProfileRow } from '.';
import { CardLayout, withApolloQuery } from '..';

function UserProfile({ data }) {
  const {
    address,
    company,
    email,
    phone,
    name: username,
  } = data.user;

  return (
    <CardLayout>
      <h1 className="text-3xl font-bold pt-8 lg:pt-0">{username}</h1>
      <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
      <ProfileRow title="Adress:" subtitle={`${address.street} ${address.suite} ${address.city}`} />
      <ProfileRow title="Email:" subtitle={email} classNames={{ title: 'mt-4' }} />
      <ProfileRow title="Phone:" subtitle={phone} classNames={{ title: 'mt-4' }} />
      <ProfileRow title="Company:" subtitle={company.name} classNames={{ title: 'mt-4' }} />
    </CardLayout>
  );
}

const GET_USER = gql`
{
  user(id: 1) {
    id
    name
    email
    phone
    company {
      name
    }
    address {
      street
      suite
      city
    }
  }
}
`;

export default withApolloQuery(UserProfile, GET_USER);
