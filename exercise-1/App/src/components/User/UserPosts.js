/* eslint-disable react/prop-types */

import React from 'react';
import { gql } from '@apollo/client';
import { withApolloQuery } from '..';
import { Subtitle as PostTitle } from '.';

function UserPosts({ data }) {
  const { name: username, posts: { data: posts } } = data.user;

  const userPosts = posts.map((post) => {
    const postTitle = `Title: ${post.title}`;

    return (
      <PostTitle key={post.id} text={postTitle} />
    );
  });

  return (
    <div className="w-full lg:w-2/5 mx-6 lg:mx-0 h-screen py-12">
      <div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">{username}&apos;s Posts:</p>
        {userPosts}
      </div>
    </div>
  );
}

const GET_POST = gql`
{
  user(id: 1) {
    id
    name
    posts (
      options: {
        paginate: {
          page: 1
          limit: 10
        }
      }) {
        data {
          id
          title
        }
    }
  }
}
`;

export default withApolloQuery(UserPosts, GET_POST);
