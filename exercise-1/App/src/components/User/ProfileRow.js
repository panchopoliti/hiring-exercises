import React from 'react';
import PropTypes from 'prop-types';
import { Title, Subtitle } from '.';

export default function ProfileRow({ title, subtitle, classNames }) {
  return (
    <>
      <Title text={title} className={classNames.title} />
      <Subtitle text={subtitle} className={classNames.subtitle} />
    </>
  );
}

ProfileRow.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  classNames: PropTypes.shape({
    title: PropTypes.string,
    subtitle: PropTypes.string,
  }),
};

ProfileRow.defaultProps = {
  title: 'Title',
  subtitle: 'Subtitle',
  classNames: {
    title: '',
    subtitle: '',
  },
};
