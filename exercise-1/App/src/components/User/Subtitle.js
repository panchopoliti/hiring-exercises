import React from 'react';
import PropTypes from 'prop-types';

export default function Subtitle({ text, className }) {
  return (
    <p className={`pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start ${className || ''}`}>
      {text}
    </p>
  );
}

Subtitle.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
};

Subtitle.defaultProps = {
  text: 'Subtitle',
  className: '',
};
