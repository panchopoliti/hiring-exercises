import React from 'react';
import PropTypes from 'prop-types';

export default function Title({ text, className }) {
  return (
    <p className={`pt-4 text-base font-bold flex items-center justify-center lg:justify-start ${className || ''}`}>
      {text}
    </p>
  );
}

Title.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
};

Title.defaultProps = {
  text: 'Title',
  className: '',
};
