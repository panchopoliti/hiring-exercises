import React from 'react';
import { useQuery } from '@apollo/client';
import { Loading, ErrorComponent } from '.';

const withApolloQuery = (Component, query) => (props) => {
  const { loading, error, data } = useQuery(query);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return <Component {...props} data={data} />;
};

export default withApolloQuery;
